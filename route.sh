#!/bin/bash

# to router wan port (from raspberry pi)
router_interface=eth0
router_ip_address=192.168.137.1

# to phone (easytether)
phone_interface=usb0
phone_gateway=192.168.42.129

./unroute.sh

sudo route add default gw $phone_gateway

sudo sysctl -w net.ipv4.ip_forward=1
sudo iptables -t nat -A POSTROUTING -o $phone_interface -j MASQUERADE

sudo iptables -I FORWARD -i $router_interface -o $phone_interface -s $router_ip_address/24 -m conntrack --ctstate NEW -j ACCEPT
sudo iptables -I FORWARD -m conntrack --ctstate RELATED,ESTABLISHED -j ACCEPT
sudo iptables -v -t mangle -A POSTROUTING -o $phone_interface -j TTL --ttl-set 65

